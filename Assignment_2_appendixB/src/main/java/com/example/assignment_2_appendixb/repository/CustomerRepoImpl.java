package com.example.assignment_2_appendixb.repository;

import com.example.assignment_2_appendixb.models.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of a Customer repository
 */
@Repository
public class CustomerRepoImpl implements CustomerRepository {
    private final String url;
    private final String username;
    private final String password;

    public CustomerRepoImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password){
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Customer> findAll() {
        String sql = "SELECT * FROM customer";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    @Override
    public Customer findById(Integer id) {
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        Customer tempCustomer = null;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            //Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                tempCustomer = customer;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempCustomer;
    }

    @Override
    public Customer findByName(String Name) {
        String sql = "SELECT * FROM customer WHERE first_name LIKE ?";
        Customer tempCustomer = null;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, Name);
            // Execute statement
            ResultSet result = statement.executeQuery();
            //Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                tempCustomer = customer;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempCustomer;
    }



    @Override
    public List<Customer> returnPageOfCustomers(int number, int from) {
        String sql = "SELECT * FROM customer ORDER BY customer_id LIMIT ? OFFSET ?";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, number);
            statement.setInt(2, from);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }


    @Override
    public int insert(Customer customer) {
        String sql = "INSERT INTO customer VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.id());
            statement.setString(2, customer.firstName());
            statement.setString(3, customer.lastName());
            statement.setString(4,null);
            statement.setString(5,null);
            statement.setString(6,null);
            statement.setString(7,null);
            statement.setString(8, customer.country());
            statement.setString(9, customer.postalCode());
            statement.setString(10, customer.phoneNumber());
            statement.setString(11,null);
            statement.setString(12, customer.email());

            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int update(Customer customer) {
        String sql = "UPDATE customer SET " +
                    "first_name = ?, " +
                    "last_name = ?," +
                    "country = ?," +
                    "postal_code = ?," +
                    "phone = ?," +
                    "email = ?" +
                    " WHERE customer_id = ?";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phoneNumber());
            statement.setString(6, customer.email());
            statement.setInt(7, customer.id());
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int delete(Customer customer) {
        String sql = "DELETE FROM customer WHERE customer_id = ?";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,customer.id());
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int deleteById(Integer id) {
        String sql = "DELETE FROM customer WHERE customer_id = ?";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public CustomerCountry returnCountryWithMostCustomers() {
        List<CustomerCountry> countries = new ArrayList<>();
        List<Customer> customers = findAll();

        for(Customer customer : customers){
            countries.add(new CustomerCountry(customer.country()));
        }

        for(Customer customer: customers){
            for(CustomerCountry country : countries){
                if(country.name.equals(customer.country())){
                    country.numberOfUsersPlusOne();
                }
            }
        }

        CustomerCountry mostCustomers = countries.get(0);
        for(CustomerCountry country : countries){
            if(mostCustomers.numberOfUsers < country.numberOfUsers){
                mostCustomers = country;
            }
        }
        return mostCustomers;
    }

    @Override
    public Customer returnHighestSpender() {
        List<Customer> customers = findAll();
        String sql = "SELECT * FROM invoice";
        List<CustomerSpender> spendings = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                CustomerSpender spending = new CustomerSpender(
                        result.getInt("customer_id"),
                        result.getDouble("total")
                );
                spendings.add(spending);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //key value pair list, first value customer_id, second value total spendings
        HashMap<Integer,Double> totalSpendings = new HashMap<>();
        for(Customer custumer : customers){
            totalSpendings.put(custumer.id(),0.0);
            for(CustomerSpender spending : spendings){
                if(spending.customerId() == custumer.id()){
                    //updates the value of an existing item in the map, by puting a new one in and replacing the previous
                    totalSpendings.put(custumer.id(),totalSpendings.get(custumer.id())+spending.total());
                }
            }
        }

        int highestSpenderId = 0;
        for(Map.Entry<Integer,Double> entry : totalSpendings.entrySet()){
            if(totalSpendings.get(highestSpenderId + 1) < entry.getValue()){
                highestSpenderId = entry.getKey();
            }
        }

        return customers.get(highestSpenderId);
    }

    @Override
    public Genre returnMostPopularGenre(Customer customer) {
        CustomerGenre customerGenre = new CustomerGenre(customer.id());
        List<InvoiceLine> invoiceLines = returnAllInvoiceLine();
        List<Invoice> invoices = returnAllInvoice();
        List<Track> tracks = returnAllTrack();
        List<Genre> genres = returnAllGenre();

        for(Invoice invoice : invoices){
            if(invoice.customerId() == customerGenre.customerId){
                customerGenre.invoiceIds.add(invoice.invoiceId());
            }
        }

        for(InvoiceLine invoiceLine : invoiceLines){
            for(int invoiceId : customerGenre.invoiceIds){
                if(invoiceLine.invoiceId() == invoiceId){
                    customerGenre.trackIds.add(invoiceLine.trackId());
                }
            }
        }

        for(Track track: tracks){
            for(int trackID : customerGenre.trackIds){
                if(track.trackId() == trackID){
                    customerGenre.genreIds.add(track.genreId());
                }
            }
        }

        for(Genre genre : genres){
            for(int genreId : customerGenre.genreIds)
                if(genre.genreId == genreId){
                    genre.timesPurchased ++;
                }
        }

        Genre favouriteGenre = genres.get(0);
        for(Genre genre : genres){
            if(favouriteGenre.timesPurchased < genre.timesPurchased){
                favouriteGenre = genre;
            }
        }

        return favouriteGenre;
    }

    /**
     * Reads a specified table from the database
     * @return - a List of objects read from the database
     * @throws - SQLException
     */
    public List<Invoice> returnAllInvoice(){
        String sql = "SELECT * FROM invoice";
        List<Invoice> invoices = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Invoice invoice = new Invoice(
                        result.getInt("invoice_id"),
                        result.getInt("customer_id")
                );
                invoices.add(invoice);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return invoices;
    }

    /**
     * Reads a specified table from the database
     * @return - a List of objects read from the database
     * @throws - SQLException
     */
    public List<InvoiceLine> returnAllInvoiceLine(){
        String sql = "SELECT * FROM invoice_line";
        List<InvoiceLine> invoiceLines = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                InvoiceLine invoiceLine = new InvoiceLine(
                        result.getInt("invoice_id"),
                        result.getInt("track_id")
                );
                invoiceLines.add(invoiceLine);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return invoiceLines;
    }

    /**
     * Reads a specified table from the database
     * @return - a List of objects read from the database
     * @throws - SQLException
     */
    public List<Track> returnAllTrack(){
        String sql = "SELECT * FROM track";
        List<Track> tracks = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Track track = new Track(
                        result.getInt("track_id"),
                        result.getInt("genre_id")
                );
                tracks.add(track);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tracks;
    }

    /**
     * Reads a specified table from the database
     * @return - a List of objects read from the database
     * @throws - SQLException
     */
    public List<Genre> returnAllGenre(){
        String sql = "SELECT * FROM genre";
        List<Genre> genres = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Genre genre = new Genre(
                        result.getInt("genre_id"),
                        result.getString("name")
                );
                genres.add(genre);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genres;
    }

}
