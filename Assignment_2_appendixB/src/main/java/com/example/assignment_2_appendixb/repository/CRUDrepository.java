package com.example.assignment_2_appendixb.repository;

import java.util.List;

/**
 * Create Read Update Delet data structure interface
 *
 * @param <T> - the type of the object that the repository consists of
 * @param <U> - the type of the ID, usually integer
 */
public interface CRUDrepository<T, U> {
    /**
     * Reads a specified table from the database
     * @return - a List of objects read from the database
     * @throws - SQLException
     */
    List<T> findAll();
    /**
     * Reads a single object
     * @return - a single object specified by id
     * @throws - SQLException
     */
    T findById(U id);

    /**
     * Adds the given object to database
     * @param - the object to insert into the database
     * @return - 1 if the insert is successful, otherwise 0
     * @throws - SQlException
     */
    int insert(T object);
    /**
     * Updates a given object in the database
     * @param - the updated object
     * @return - 1 if the update is successful, otherwise 0
     * @throws - SQlException
     */
    int update(T object);
    /**
     * Deletes the given object from database
     * @param - the object to insert into the database
     * @return - 1 if the delete is successful, otherwise 0
     * @throws - SQlException
     */
    int delete(T object);
    /**
     * Deletes the given object by id from database
     * @param - the object to insert into the database
     * @return - 1 if the delete is successful, otherwise 0
     * @throws - SQlException
     */
    int deleteById(U id);

}

