package com.example.assignment_2_appendixb.repository;

import com.example.assignment_2_appendixb.models.Customer;
import com.example.assignment_2_appendixb.models.CustomerCountry;
import com.example.assignment_2_appendixb.models.CustomerGenre;
import com.example.assignment_2_appendixb.models.Genre;

import java.util.List;

/**
 * CustomerRepository interface for customers that extends the CRUD data structure
 */

public interface CustomerRepository extends CRUDrepository<Customer,Integer> {
    /**
     * Reads a single object from the database
     * @param Name - the name of the customer we want to return
     * @return - a single object specified by the Name
     * @throws - SQLException
     */
    Customer findByName(String Name);
    //returns the specified *number* of customers, starting *from* the specified index

    /**
     * returns the specified number of customers
     * @param number - the number of customers we want to return
     * @param from - the starting index of the returnable customers
     * (if number = 3 and from = 5, the method will return the customers 6-7-8)
     * @return - the specified number of customers
     * @throws - SQLException
     */
    List<Customer> returnPageOfCustomers(int number,int from);

    /**
     * @return - the CustomerCountry object of the country that has the most customers
     */
    CustomerCountry returnCountryWithMostCustomers();

    /**
     * @return - a Customer that spent the most on music tracks
     */
    Customer returnHighestSpender();

    /**
     *  Returns the most popular genre for the specified costumer
     * @param customer - the customer whose favourite genre we want
     * @return - the customers most purchased genre
     */
    Genre returnMostPopularGenre(Customer customer);
}
