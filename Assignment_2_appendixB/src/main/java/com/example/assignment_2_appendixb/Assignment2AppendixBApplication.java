package com.example.assignment_2_appendixb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment2AppendixBApplication {

    public static void main(String[] args) {
        SpringApplication.run(Assignment2AppendixBApplication.class, args);
    }

}
