package com.example.assignment_2_appendixb.runner;

import com.example.assignment_2_appendixb.models.Customer;
import com.example.assignment_2_appendixb.models.CustomerCountry;
import com.example.assignment_2_appendixb.repository.CustomerRepoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Runner app to test all the customer repository
 */
@Component
public class chinookAppRunner implements ApplicationRunner {

    private final CustomerRepoImpl customerRepo;

    @Autowired
    public chinookAppRunner(CustomerRepoImpl customerRepo){
        this.customerRepo = customerRepo;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<Customer> customers = customerRepo.findAll();
        System.out.println(customers);

        Customer customerByID = customerRepo.findById(34);
        System.out.println(customerByID);

        Customer customerbyName = customerRepo.findByName("Hannah");
        System.out.println(customerbyName);

        List<Customer> customersFrom5til8 = customerRepo.returnPageOfCustomers(3,5);
        System.out.println(customersFrom5til8);

        Customer customerToInsert = new Customer(
                60,
                "Test",
                "Test",
                "Hungary",
                "5200",
                "+36 20 8888888",
                "test@gmail.com"
                );
        if(customerRepo.insert(customerToInsert) == 1){
            Customer customerAdded = customerRepo.findById(60);
            System.out.println(customerAdded);
        }

        Customer customerUpdated = new Customer(
                60,
                "Updated",
                "Updated",
                "Updatedcountry",
                "11111",
                "+36 2011 8888888",
                "updated@gamil.com"
        );
        if(customerRepo.update(customerUpdated) == 1){
            Customer customerAdded = customerRepo.findById(60);
            System.out.println(customerAdded);
        }

        customerRepo.deleteById(60);

        CustomerCountry c = customerRepo.returnCountryWithMostCustomers();
        System.out.println(c.name + " has the most costumers.");

        Customer highestSpender = customerRepo.returnHighestSpender();
        System.out.println(highestSpender.firstName() + " is the highest spender");

        System.out.println(highestSpender.firstName() + "'s favourite music genre is: " +
                customerRepo.returnMostPopularGenre(highestSpender).genreName);

    }
}
