package com.example.assignment_2_appendixb.models;

/**
 * A country Object
 *
 * This Object is for keeping track of how many users each country has.
 * The name attribute is the country's name.
 * the numberOfUsers attribute represents the number of customers that are from that country.
 */
public class CustomerCountry{

    public String name;
    public int numberOfUsers;

    /**
     * Constructor
     * @param name - the name of the country
     * The numberOfUsers attribute is zero on object creation
     */
    public CustomerCountry(String name){
        this.name = name;
        this.numberOfUsers = 0;
    }

    /**
     * increments the numberOfUsers attribute by one
     */
    public void numberOfUsersPlusOne(){
        this.numberOfUsers ++;
    }
}
