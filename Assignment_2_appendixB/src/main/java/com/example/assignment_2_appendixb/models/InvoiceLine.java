package com.example.assignment_2_appendixb.models;

/**
 * InvoiceLine object
 *
 * This invoiceline object represents a line from the invoice_line table, where it's stored that wich invoice consist of wich music track purchases
 * @param invoiceId - the id of the incoive
 * @param trackId - the id of the track that was purchased
 */
public record InvoiceLine(int invoiceId,int trackId) {
}
