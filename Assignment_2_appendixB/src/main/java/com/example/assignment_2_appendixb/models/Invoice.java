package com.example.assignment_2_appendixb.models;

/**
 * Invoice object
 *
 * This invoice object represents an invoice(purchase) for a customer
 *
 * @param invoiceId = the id of an invoice
 * @param customerId = the users id
 */
public record Invoice(int invoiceId,int customerId) {
}
