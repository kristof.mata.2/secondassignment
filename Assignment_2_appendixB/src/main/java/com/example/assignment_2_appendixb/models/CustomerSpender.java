package com.example.assignment_2_appendixb.models;

/**
 * CustomerSpender object
 *
 *This object is for keeping track of the spendings of the customers.
 *
 * @param customerId - the id of the customer
 * @param total - the total that they paid in one purchase
 */
public record CustomerSpender(int customerId, double total) {

}
