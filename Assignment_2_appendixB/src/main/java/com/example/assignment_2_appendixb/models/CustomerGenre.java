package com.example.assignment_2_appendixb.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Customer Genre object
 *
 * This object has all the necessary parameters for returning a customers favourite genre
 *  customerId attribute is the id of the customer whose favourite genre needs to be returned
 *  invoiceIds attribute is the list of the purchase ids this customer has from the invoice table
 *  trackIds attribute is the list of the music track ids that this customer purchased
 *  genreIds attribute is the list of the ids of the genre from those tracks that the customer purchased
 */
public class CustomerGenre {
    public int customerId;
    public List<Integer> invoiceIds;
    public List<Integer> trackIds;
    public List<Integer> genreIds;

    /**
     *
     * @param id - the customers id
     * the List attributes are empty lists on creation
     */
    public CustomerGenre(int id){
        this.customerId = id;
        this.invoiceIds = new ArrayList<>();
        this.trackIds = new ArrayList<>();
        this.genreIds = new ArrayList<>();
    }


}
