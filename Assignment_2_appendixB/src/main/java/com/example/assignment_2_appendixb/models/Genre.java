package com.example.assignment_2_appendixb.models;

/**
 * Genre Object
 *
 * This is a genre object for each music genre.
 *  genreId attribute is the id of each genre.
 *  genreName attribute is the name of each genre.
 *  timesPurchased attribute, is the number of times a music of that genre been purchased by the specific customer
 */


public class Genre {
    public int genreId;
    public String genreName;
    public int timesPurchased;

    /**
     * Constructor
     * @param id - id of the genre
     * @param name - name of the genre
     */
    public Genre(int id, String name){
        this.genreId = id;
        this.genreName = name;
        this.timesPurchased = 0;
    }
}
