package com.example.assignment_2_appendixb.models;
/**
 * Customer Record Object
 *
 * Contains the parameters of a customer for the Chinook database.
 * Since this is a record, there are no constructors.
 * The parameters:
 * @param id - the id number of the customer
 * @param firstName - first name of the customer
 * @param lastName - last name of the customer
 * @param country - the country where the customer is from
 * @param postalCode - the postal code of the customer
 * @param phoneNumber - the phonenumber of the customer
 * @param email -the email addres of the customer
 */
public record Customer(
        int id,
        String firstName,
        String lastName,
        String country,
        String postalCode,
        String phoneNumber,
        String email
){}
