package com.example.assignment_2_appendixb.models;

/**
 * Track object
 *
 * this object represents a music track
 * @param trackId - the id of the track
 * @param genreId - the id of the track's genre
 */
public record Track(int trackId,int genreId) {
}
