## Name
Assigment 2

## Description
The first part deals with basic SQL database manipulating functions in postgreSQL.
The second part of the assignment deals with manipulating SQL data in Spring using a the JDBC with the PostgreSQL 
driver.

## Application
![image.png](./image.png)
![image-1.png](./image-1.png)

## Installation
Install JDK 17+
Install PostgreSQL - pgAdmin 4
Import chimook database
Install Intellij
Clone repository
Modify application.properties with your pgAdmin 4 password.
Make sure in application.properties the database url points to a valid database.

## Usage
Run the application
Use the menu shown on screen.

## Maintainers
@tothbt
@kristof.mata.2

## Project status
Development has slowed down or stopped completely.
