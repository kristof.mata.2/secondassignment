INSERT INTO power (pow_name, pow_desc) VALUES ('flying','Hero can fly.');
INSERT INTO power (pow_name, pow_desc) VALUES ('super power','Hero has super strength.');
INSERT INTO power (pow_name, pow_desc) VALUES ('costume','Hero has superhero costume.');
INSERT INTO power (pow_name, pow_desc) VALUES ('laser eyes','Hero has laser eyes.');

--ironman and superman can fly
INSERT INTO superhero_power (sup_id, pow_id) VALUES (1,1);
INSERT INTO superhero_power (sup_id, pow_id) VALUES (2,1);
--superman has super strength and laser eyes
INSERT INTO superhero_power (sup_id, pow_id) VALUES (2,2);
INSERT INTO superhero_power (sup_id, pow_id) VALUES (2,4);
--all heroes have a superhero custume
INSERT INTO superhero_power (sup_id, pow_id) VALUES (1,3);
INSERT INTO superhero_power (sup_id, pow_id) VALUES (2,3);
INSERT INTO superhero_power (sup_id, pow_id) VALUES (3,3);
