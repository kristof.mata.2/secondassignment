DROP TABLE IF EXISTS superhero_power;

CREATE TABLE superhero_power (
	sup_id int REFERENCES superhero NOT NULL,
	pow_id int REFERENCES power NOT NULL,
	PRIMARY KEY (sup_id, pow_id)
);

