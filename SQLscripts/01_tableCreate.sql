DROP TABLE IF EXISTS superhero;

CREATE TABLE superhero (
    sup_id serial PRIMARY KEY,
    sup_name varchar(50) NOT NULL,
    sup_alias varchar(50) NOT NULL,
	sup_origin varchar(50) NOT NULL
);

DROP TABLE IF EXISTS assistant;

CREATE TABLE assistant (
    assi_id serial PRIMARY KEY,
    assi_name varchar(50) NOT NULL
);

DROP TABLE IF EXISTS power;

CREATE TABLE power (
    pow_id serial PRIMARY KEY,
    pow_name varchar(50) NOT NULL,
	pow_desc varchar(50) NOT NULL
);